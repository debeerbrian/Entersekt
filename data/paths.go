package data

import (
	"encoding/json"
	"io"

	"github.com/go-playground/validator"
)

// Path data structure for the GET request
type Path struct {
	Path string `json:"path" validate:"required"`
}

// FromJSON deserializes the object from JSON string
func (p *Path) FromJSON(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(p)
}

// Validate Path data structure
func (p *Path) Validate() error {
	validate := validator.New()
	return validate.Struct(p)
}
