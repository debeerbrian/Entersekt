package data

import "testing"

func TestCheckValidation(t *testing.T) {
	validPath := &Path{Path: "."}
	err := validPath.Validate()
	if err != nil {
		t.Fatal(err)
	}

	invalidPath := &Path{}
	err = invalidPath.Validate()
	if err != nil {
		return
	}
	t.Fatal(err)
}
