# Entersekt Technical Assessment

## Deployment

Ensure that docker is installed on your system and run the deploy script:

```bash
sudo ./docker_deploy
```

## Using the API

The API can be interfaced with on port `8080` using cURL as follows:

```bash
curl -v 127.0.0.1:8080/listings -X GET -d '{"path": <your_path>}'
```

Furthermore to get a prettier output is recommended to use [jq](https://stedolan.github.io/jq/download/) a json processor:

```bash
curl -v 127.0.0.1:8080/listings -X GET -d '{"path": "."}' | jq
```

## Testing

A few test check were written to ensure that data object are processed appropriately. The tests can be executed as follows:

```bash
go test ./data
```