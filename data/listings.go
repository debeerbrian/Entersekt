package data

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Listing defines a object of the directory listing
type Listing struct {
	FullPath   string `json:"path"`
	Size       int64  `json:"size"`
	Attributes string `json:"attributes"`
}

// FromJSON deserializes the object from JSON string
func (li *Listing) FromJSON(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(li)
}

// Listings defines a slice of Dir
type Listings []*Listing

// ToJSON serializes the contents of the collection to JSON
func (li *Listings) ToJSON(w io.Writer) error {
	e := json.NewEncoder(w)
	return e.Encode(li)
}

// GetListings returns all listings in a directory
func GetListings(walkPath *Path) Listings {
	var listings Listings
	var path2start string = ""
	var err error

	if strings.HasPrefix(walkPath.Path, ".") { // relative path
		path2start, err = os.Getwd()
		if err != nil {
			log.Println(err)
		}
		path2start = path2start + "/"
	}
	err = filepath.Walk(walkPath.Path,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			unixPerms := info.Mode() & os.ModePerm
			permString := fmt.Sprintf("%v", unixPerms)
			fullpath := path2start + path
			listing := Listing{FullPath: fullpath, Size: info.Size(), Attributes: permString}
			listings = append(listings, &listing)

			return nil
		})
	if err != nil {
		log.Println(err)
	}
	return listings
}
