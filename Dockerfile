## Specify base image
FROM golang:1.16.0-alpine3.13
## Create app directory
RUN mkdir /app
## Copy root directory into /app directory
ADD . /app
## Set work directory for following executions
WORKDIR /app
## Install dependancies
RUN go mod download
## Compile application
RUN go build -o main .
## Expose API port
EXPOSE 8080
## Start binary
CMD ["/app/main"]
