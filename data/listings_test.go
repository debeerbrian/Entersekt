package data

import (
	"testing"
)

func TestGetListings(t *testing.T) {

	testPathString := "test_dir"
	testPath := &Path{Path: testPathString}
	listings := GetListings(testPath)

	if listings[1].FullPath != "test_dir\\test_resource.txt" ||
		listings[1].Size != 8 ||
		listings[1].Attributes != "-rw-rw-rw-" {
		t.Fail()
	}
}
