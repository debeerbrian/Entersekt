package handlers

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/debeerbrian/Entersekt/data"
)

// Listings is a http.Handler
type Listings struct {
	l *log.Logger
}

// NewListings returns a new listings handler with the given logger
func NewListings(l *log.Logger) *Listings {
	return &Listings{l}
}

// GetListings returns all the listing from the provided path
func (li *Listings) GetListings(rw http.ResponseWriter, r *http.Request) {
	li.l.Println("Handle GET Listings")

	getPath := r.Context().Value(KeyListings{}).(data.Path)
	// walk through the directory and return all the listings
	lp := data.GetListings(&getPath)

	// serialize the list to JSON
	err := lp.ToJSON(rw)
	if err != nil {
		li.l.Println("Unable to marshal json")
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

// KeyListings is a key used for the Path object in the context
type KeyListings struct{}

// MiddlewareValidateListings validates the path coming in for a listing request
func (li *Listings) MiddlewareValidateListings(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		getPath := data.Path{}

		// valiidate that valid json was provided
		err := getPath.FromJSON(r.Body)
		if err != nil {
			li.l.Println("[ERROR] deserializing path", err)
			http.Error(rw, "Error deserializing path", http.StatusBadRequest)
			return
		}

		// validate that the json object passed is a valid Path object
		err = getPath.Validate()
		if err != nil {
			li.l.Println("[ERROR] validating path", err)
			http.Error(rw, fmt.Sprintf("Error validating path %s", err), http.StatusBadRequest)
			return
		}

		// add the path to the context
		ctx := context.WithValue(r.Context(), KeyListings{}, getPath)
		r = r.WithContext(ctx)

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(rw, r)
	})
}
